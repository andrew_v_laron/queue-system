const { MongoClient } = require('mongodb');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'queue';
var db;
// Use connect method to connect to the server

const connectDB=function(callback){
  MongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
    if (!err) {
      db = client.db(dbName);
    } else console.error(err);
    callback(err);
  });
}

const disconnectDB=function(){
  MongoClient.close();
}

const getDB=function(){
  return db;
}


module.exports={getDB, connectDB,disconnectDB}