var express = require('express');
var router = express.Router();

const {getIndexPage,getQueuePage,getReceptionPage}=require('../controllers/PageController')

/* GET landing page. */
router.get('/', getIndexPage);

/* GET queue page. */
router.get('/queue', getQueuePage);

/* GET receptionist's page. */
router.get('/reception', getReceptionPage);

module.exports = router;
