var express = require('express');
var router = express.Router();

const {enqueue,dequeue,serve,findQueueEntries} = require('../controllers/QueueEntryController')
/* GET queue entries listing. */
router.get('/', findQueueEntries);

/* Create a Queue Entry */
router.post('/enqueue', enqueue);

router.post('/dequeue', dequeue);

router.post('/serve', serve);

module.exports = router;
