const { getDB } = require('../util/mongodb');
const queueEntries = getDB().collection('queueentries');

const findQueueEntries = function (req, res) {
    queueEntries.find({}).toArray(function (err, docs) {
        if (!err) {
            res.send(docs);
        }else {
            console.error(err);
            res.status(500).send(err);
        }
    });
}

const enqueue = function (req, res) {
    let params=req.body;
    queueEntries.find({}).sort({_id:-1}).limit(1).toArray(function(err,docs){
        if(docs.length)
            params.number=docs[0].number+1;
        else params.number=1;
        queueEntries.insert(params, function (err, result) {
            if (!err) {
                res.redirect('/queue?name=' + params.name + "&number=" + params.number);
            } else {
                console.error(err);
                res.status(500).send(err);
            }
        })  
    }) 
}

const dequeue = function (req, res) {
    queueEntries.deleteOne({ name:req.body.name }, function (err, result) {
        if (!err) {
            res.redirect('/');
        } else {
            console.error(err);
            res.status(500).send(err);
        }
    }); 
}

const serve = function (req, res) {
    queueEntries.deleteOne({}, function (err, result) {
        if (!err) {
            res.redirect('/reception');
        } else {
            console.error(err);
            res.status(500).send(err);
        }
    });    
}

module.exports={
    findQueueEntries,
    enqueue,
    dequeue,
    serve
}