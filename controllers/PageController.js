const {getDB}=require('../util/mongodb');
const queueEntries = getDB().collection('queueentries');

const getIndexPage = function (req, res) {
  res.render('index', { title: 'Customer Registration' });
}

const getQueuePage = function (req, res) {
  queueEntries.find({}).toArray(function (err, docs) {
    if (!err) {
      res.render('queue', {
        title: 'Queue System',
        username: req.query.name,
        userQueueNumber: req.query.number,
        queueEntries: docs
      });
    } else {
      console.error(err);
      res.status(500).send(err);
    }
  });
  
}

const getReceptionPage=function (req, res) {
  queueEntries.find({}).toArray(function (err, docs) {
    if (!err) {
      res.render('reception', { title: 'Receptionist\'s Page', queueEntries: docs, nowServing: docs.length ? docs[0].number : 'None' });
    } else {
      console.error(err);
      res.status(500).send(err);
    }
  });
 
}

module.exports={
  getIndexPage,
  getQueuePage,
  getReceptionPage
}