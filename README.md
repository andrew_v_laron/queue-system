# Queue System application

## Overview

This project demonstrates using Node.js, Express.js, and MongoDB to create a simple web application for tracking customer queues, for example at a bank or government office.

## Branches

`step0-generator` : what the code looks like after running the Express application generator

`step1-views` : code after setting up views, styles, and routes to render views

`step2-routes` : code after creating controllers and REST routes

`step3-db` : code after integrating MongoDB for tracking the queue